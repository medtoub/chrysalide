classdef RT2DJS_V0_5 < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        TrouveTaVoieV07UIFigure  matlab.ui.Figure
        InputEditFieldLabel      matlab.ui.control.Label
        InputEditField           matlab.ui.control.EditField
        ScanButton               matlab.ui.control.Button
        TabGroup                 matlab.ui.container.TabGroup
        Tab                      matlab.ui.container.Tab
        UITable                  matlab.ui.control.Table
        UITable_2                matlab.ui.control.Table
        Label                    matlab.ui.control.Label
        UITable_9                matlab.ui.control.Table
        Tab_2                    matlab.ui.container.Tab
        UITable_3                matlab.ui.control.Table
        UITable_4                matlab.ui.control.Table
        Label_2                  matlab.ui.control.Label
        UITable_10               matlab.ui.control.Table
        Tab_4                    matlab.ui.container.Tab
        UITable_5                matlab.ui.control.Table
        UITable_6                matlab.ui.control.Table
        Label_3                  matlab.ui.control.Label
        UITable_11               matlab.ui.control.Table
        Tab_3                    matlab.ui.container.Tab
        UITable_7                matlab.ui.control.Table
        UITable_8                matlab.ui.control.Table
        Label_4                  matlab.ui.control.Label
        UITable_12               matlab.ui.control.Table
        Tab_5                    matlab.ui.container.Tab
        UITable_13               matlab.ui.control.Table
        UITable_14               matlab.ui.control.Table
        Label_5                  matlab.ui.control.Label
        UITable_15               matlab.ui.control.Table
        Knob                     matlab.ui.control.Knob
    end

    
    properties (Access = private)
            emb1  % variable
            net1  % variable
            FCod1  % variable
            ROME2All1  % variable
            ROME2Jobs1  % variable
            ROME1  % variable
            LHEO1  % variable
            Tags1  % variable
            %Seuil % variable
    end
    
    properties (Access = public)
        UIAxes            matlab.ui.control.UIAxes
        UIAxes_1            matlab.ui.control.UIAxes
        UIAxes_2            matlab.ui.control.UIAxes
        UIAxes_3            matlab.ui.control.UIAxes
        UIAxes_4            matlab.ui.control.UIAxes
        UIAxes_5            matlab.ui.control.UIAxes
        UIAxes_6            matlab.ui.control.UIAxes
        UIAxes_7            matlab.ui.control.UIAxes
        UIAxes_8            matlab.ui.control.UIAxes
        UIAxes_9            matlab.ui.control.UIAxes
        UIAxes_10            matlab.ui.control.UIAxes
        UIAxes_11            matlab.ui.control.UIAxes
        UIAxes_12            matlab.ui.control.UIAxes
        UIAxes_13            matlab.ui.control.UIAxes
        UIAxes_14            matlab.ui.control.UIAxes
        UIAxes_15            matlab.ui.control.UIAxes
    end
    

    % Callbacks that handle component events
    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app)
            data = load('CNNFormacode3.mat');
            app.emb1 = data.emb;
            app.net1 = data.net;
            app.FCod1 = data.FCod;
            app.ROME2All1 = data.ROME2All;
            app.ROME2Jobs1 = data.ROME2Jobs;
            app.ROME1 = data.ROME;
            app.LHEO1 = data.LHEO;
            app.Tags1 = data.Tags;          
        end

        % Button pushed function: ScanButton
        function ScanButtonPushed(app, event)
            emb = app.emb1;
            net = app.net1;
            FCod = app.FCod1;
            ROME2All = app.ROME2All1;
            ROME2Jobs = app.ROME2Jobs1;
            ROME = app.ROME1;
            LHEO = app.LHEO1;
            Tags = app.Tags1;
            
            
            %% Input
            InputNew = app.InputEditField.Value;
            Seuil = app.Knob.Value/100;
            
            %% Preprocess the input
            XNew = transformInputData(InputNew,100,emb);
            
            %% Classification
            [labelsNew,score] = classify(net,cell2mat(XNew));
            

            Result = table;
            Result.Tags = Tags;
            Result.Scores = score';
            Result = sortrows(Result,'Scores','descend');

            ROMEarray = Result{Result.Scores>Seuil,1};

              app.UITable.Data = table; 
              app.UITable_2.Data = table;
              app.UITable_3.Data = table; 
              app.UITable_4.Data = table;
              app.UITable_5.Data = table; 
              app.UITable_6.Data = table;
              app.UITable_7.Data = table; 
              app.UITable_8.Data = table;
              app.UITable_9.Data = table; 
              app.UITable_10.Data = table;
              app.UITable_11.Data = table; 
              app.UITable_12.Data = table;
            
            N = min(size(ROMEarray,1),5);
            
            Formations = struct;

            for i=1:N
                SkillsTable = table;
                SkillsTable.CodesROME = string(ROMEarray(i));
                SkillsTable = innerjoin(SkillsTable,ROME2All,'LeftKeys',1,'RightKeys',1);
                
%                 IdRows = [];
                IdRows = any((strcmp(LHEO.CodeROME1,ROMEarray(i))|strcmp(LHEO.CodeROME2,ROMEarray(i))|strcmp(LHEO.CodeROME3,ROMEarray(i))),2);
                Formations(i).FormationTable = LHEO(IdRows,[9 42 38 44 31 19 14]);
                
                JobsTable = table;
                JobsTable.CodesROME = string(ROMEarray(i));
                JobsTable = innerjoin(JobsTable,ROME2Jobs,'LeftKeys',1,'RightKeys',1);
                url = ['https://candidat.pole-emploi.fr/marche-du-travail/statistiques?codeMetier=' num2str(JobsTable{1,3}) '&codeZoneGeographique=75&typeZoneGeographique=DEPARTEMENT'];
                options = weboptions('Timeout',600);
                code = webread(url,options);
                tree = htmlTree(code);
                try
                    selector = '#pieChart1 li';
                    subtrees = findElement(tree,selector);
                    Contracts = extractHTMLText(subtrees);
                    LabelsContracts = repmat({''},1,size(Contracts,1));
                    Percentages = double(extractBefore(extractHTMLText(subtrees),"% - "));
                catch
                end
                try
                    selector = '.media-bd > ul:nth-child(3) > li';
                    subtrees = findElement(tree,selector);
                    if ~strcmp(extractAfter(extractHTMLText(subtrees),"Pour les 12 derniers mois : "),"Données Pôle emploi insuffisantes pour le calcul")
                        Demands = double(extractBetween(extractHTMLText(subtrees),"pour "," demandeu"));
                        Offers = double(extractBetween(extractHTMLText(subtrees),": "," offre"));
                        Period = 'Yearly ';
                    else
                    selector = '.media-bd > ul:nth-child(1) > li';
                    subtrees = findElement(tree,selector);
                        Demands = double(extractBetween(extractHTMLText(subtrees),"pour "," demandeu"));
                        Offers = double(extractBetween(extractHTMLText(subtrees),": "," offre"));
                        Period = 'Monthly ';
                    end
                catch
                end

                Age = categorical(["Under 35 Years Old" "Over 35 Years Old"]);
                selector = 'td.euro';
                subtrees = findElement(tree,selector);
                Salary = [double(extractBetween(extractHTMLText(subtrees),"De ","€")) double(extractBetween(extractHTMLText(subtrees),"à ","€"))];
                Salary(:,2) = Salary(:,2) - Salary(:,1);

                
                
                if i == 1
                    app.Tab.Title = ROME{strcmp(ROME.code_rome,ROMEarray(i)),2};
                    app.Label.Text = ROME{strcmp(ROME.code_rome,ROMEarray(i)),3};
                    app.UITable.Data = unique(JobsTable(:,[2]));  
                    app.UITable_2.Data = unique(SkillsTable(:,[2]));
                    app.UITable_9.Data = Formations(i).FormationTable;
                    app.UITable_9.ColumnName = {'Diplôme', 'Type', 'Durée', 'Etablissement', 'Affiliation', 'Ville', 'Rythme'};
                    % Create UIAxes
                    app.UIAxes = uiaxes(app.Tab);
                    title(app.UIAxes,'Contract Types');
%                     app.UIAxes.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes.Position = [16 1 430 223];
                    try
                    p = pie(app.UIAxes,Percentages,LabelsContracts);
                    set(p,'edgecolor','none');
                    legend(app.UIAxes,Contracts,'Location','southoutside','Orientation','vertical','FontSize',6);
                    catch
                    end
                    % Create UIAxes_2
                    app.UIAxes_2 = uiaxes(app.Tab);
                    title(app.UIAxes_2,[Period 'Average Job Offers & Demands']);
%                     app.UIAxes_2.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_2.Position = [464 1 430 223];
                    bar(app.UIAxes_2,categorical({'Job Demand','Job Offers'}),[Demands;Offers],'EdgeColor','none');
                    xlabel(app.UIAxes_2,'');
                    ylabel(app.UIAxes_2,'');
                    % Create UIAxes_3
                    app.UIAxes_3 = uiaxes(app.Tab);
%                     app.UIAxes_3.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_3.Position = [918 1 430 223];
                    b = bar(app.UIAxes_3,Age,Salary,'stacked','FaceColor',[0 0.69 0.31],'EdgeColor','none');
                    b(1).FaceColor = 'none';
                    xlabel(app.UIAxes_3,'');
                    ylabel(app.UIAxes_3,'Salary (€)');
                    title(app.UIAxes_3,'Salaries (Paris)');
                end
                
                if i == 2
                    app.Tab_2.Title = ROME{strcmp(ROME.code_rome,ROMEarray(i)),2};
                    app.Label_2.Text = ROME{strcmp(ROME.code_rome,ROMEarray(i)),3};
                    app.UITable_3.Data = unique(JobsTable(:,[2]));  
                    app.UITable_4.Data = unique(SkillsTable(:,[2]));
                    app.UITable_10.Data = Formations(i).FormationTable;
                    app.UITable_10.ColumnName = {'Diplôme', 'Type', 'Durée', 'Etablissement', 'Affiliation', 'Ville', 'Rythme'};
                    % Create UIAxes_4
                    app.UIAxes_4 = uiaxes(app.Tab_2);
                    app.UIAxes_4.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_4.Position = [16 1 430 223];
                    % Create UIAxes_5
                    app.UIAxes_5 = uiaxes(app.Tab_2);
                    app.UIAxes_5.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_5.Position = [464 1 430 223];
                    % Create UIAxes_6
                    app.UIAxes_6 = uiaxes(app.Tab_2);
                    app.UIAxes_6.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_6.Position = [918 1 430 223];
                    try
                    p = pie(app.UIAxes_4,Percentages,LabelsContracts);
                    set(p,'edgecolor','none');
                    legend(app.UIAxes_4,Contracts,'Location','southoutside','Orientation','vertical','FontSize',6);
                    catch
                    end
                    bar(app.UIAxes_5,categorical({'Job Demand','Job Offers'}),[Demands;Offers],'EdgeColor','none');
                    b = bar(app.UIAxes_6,Age,Salary,'stacked','FaceColor',[0 0.69 0.31],'EdgeColor','none');
                    b(1).FaceColor = 'none';
                    xlabel(app.UIAxes_5,'');
                    xlabel(app.UIAxes_6,'');
                    ylabel(app.UIAxes_6,'Salary (€)');
                    title(app.UIAxes_4,'Contract Types');
                    title(app.UIAxes_5,[Period 'Average Job Offers & Demands']);
                    title(app.UIAxes_6,'Salaries (Paris)');
                end
                
                if i == 3
                    app.Tab_4.Title = ROME{strcmp(ROME.code_rome,ROMEarray(i)),2};
                    app.Label_3.Text = ROME{strcmp(ROME.code_rome,ROMEarray(i)),3};
                    app.UITable_5.Data = unique(JobsTable(:,[2]));  
                    app.UITable_6.Data = unique(SkillsTable(:,[2]));
                    app.UITable_11.Data = Formations(i).FormationTable;
                    app.UITable_11.ColumnName = {'Diplôme', 'Type', 'Durée', 'Etablissement', 'Affiliation', 'Ville', 'Rythme'};
                    % Create UIAxes_7
                    app.UIAxes_7 = uiaxes(app.Tab_4);
                    app.UIAxes_7.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_7.Position = [16 1 430 223];
                    % Create UIAxes_8
                    app.UIAxes_8 = uiaxes(app.Tab_4);
                    app.UIAxes_8.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_8.Position = [464 1 430 223];
                    % Create UIAxes_9
                    app.UIAxes_9 = uiaxes(app.Tab_4);
                    app.UIAxes_9.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_9.Position = [918 1 430 223];                    
                    try
                    p = pie(app.UIAxes_7,Percentages,LabelsContracts);
                    set(p,'edgecolor','none');
                    legend(app.UIAxes_7,Contracts,'Location','southoutside','Orientation','vertical','FontSize',6);
                    catch
                    end
                    bar(app.UIAxes_8,categorical({'Job Demand','Job Offers'}),[Demands;Offers],'EdgeColor','none');
                    b = bar(app.UIAxes_9,Age,Salary,'stacked','FaceColor',[0 0.69 0.31],'EdgeColor','none');
                    b(1).FaceColor = 'none';
                    xlabel(app.UIAxes_8,'');
                    ylabel(app.UIAxes_8,'');
                    xlabel(app.UIAxes_9,'');
                    ylabel(app.UIAxes_9,'Salary (€)');
                    title(app.UIAxes_7,'Contract Types');
                    title(app.UIAxes_8,[Period 'Average Job Offers & Demands']);
                    title(app.UIAxes_9,'Salaries (Paris)');
                end
                
                if i == 4
                    app.Tab_3.Title = ROME{strcmp(ROME.code_rome,ROMEarray(i)),2};
                    app.Label_4.Text = ROME{strcmp(ROME.code_rome,ROMEarray(i)),3};
                    app.UITable_7.Data = unique(JobsTable(:,[2]));  
                    app.UITable_8.Data = unique(SkillsTable(:,[2]));
                    app.UITable_12.Data = Formations(i).FormationTable;
                    app.UITable_12.ColumnName = {'Diplôme', 'Type', 'Durée', 'Etablissement', 'Affiliation', 'Ville', 'Rythme'};
                    % Create UIAxes_10
                    app.UIAxes_10 = uiaxes(app.Tab_3);
                    app.UIAxes_10.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_10.Position = [16 1 430 223];
                    % Create UIAxes_11
                    app.UIAxes_11 = uiaxes(app.Tab_3);
                    app.UIAxes_11.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_11.Position = [464 1 430 223];
                    % Create UIAxes_12
                    app.UIAxes_12 = uiaxes(app.Tab_3);
                    app.UIAxes_12.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_12.Position = [918 1 430 223];                    
                    try
                    p = pie(app.UIAxes_10,Percentages,LabelsContracts);
                    set(p,'edgecolor','none');
                    legend(app.UIAxes_10,Contracts,'Location','southoutside','Orientation','vertical','FontSize',6);
                    catch
                    end
                    bar(app.UIAxes_11,categorical({'Job Demand','Job Offers'}),[Demands;Offers],'EdgeColor','none');
%                     figure(app.UIAxes_11);
                    b = bar(app.UIAxes_12,Age,Salary,'stacked','FaceColor',[0 0.69 0.31],'EdgeColor','none');
                    b(1).FaceColor = 'none';
                    xlabel(app.UIAxes_11,'');
                    ylabel(app.UIAxes_11,'');
                    xlabel(app.UIAxes_12,'');
                    ylabel(app.UIAxes_12,'Salary (€)');
                    title(app.UIAxes_10,'Contract Types');
                    title(app.UIAxes_11,[Period 'Average Job Offers & Demands']);
                    title(app.UIAxes_12,'Salaries (Paris)');
                end
                
                if i == 5
                    app.Tab_5.Title = ROME{strcmp(ROME.code_rome,ROMEarray(i)),2};
                    app.Label_5.Text = ROME{strcmp(ROME.code_rome,ROMEarray(i)),3};
                    app.UITable_13.Data = unique(JobsTable(:,[2]));  
                    app.UITable_14.Data = unique(SkillsTable(:,[2]));
                    app.UITable_15.Data = Formations(i).FormationTable;
                    app.UITable_15.ColumnName = {'Diplôme', 'Type', 'Durée', 'Etablissement', 'Affiliation', 'Ville', 'Rythme'};
                    % Create UIAxes_13
                    app.UIAxes_13 = uiaxes(app.Tab_5);
                    app.UIAxes_13.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_13.Position = [16 1 430 223];
                    % Create UIAxes_14
                    app.UIAxes_14 = uiaxes(app.Tab_5);
                    app.UIAxes_14.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_14.Position = [464 1 430 223];
                    % Create UIAxes_15
                    app.UIAxes_15 = uiaxes(app.Tab_5);
                    app.UIAxes_15.PlotBoxAspectRatio = [1 0.504792332268371 0.504792332268371];
                    app.UIAxes_15.Position = [918 1 430 223];                    
                    try
                    p = pie(app.UIAxes_10,Percentages,LabelsContracts);
                    set(p,'edgecolor','none');
                    legend(app.UIAxes_10,Contracts,'Location','southoutside','Orientation','vertical','FontSize',6);
                    catch
                    end
                    bar(app.UIAxes_14,categorical({'Job Demand','Job Offers'}),[Demands;Offers],'EdgeColor','none');
%                     figure(app.UIAxes_14);
                    b = bar(app.UIAxes_15,Age,Salary,'stacked','FaceColor',[0 0.69 0.31],'EdgeColor','none');
                    b(1).FaceColor = 'none';
                    xlabel(app.UIAxes_14,'');
                    ylabel(app.UIAxes_14,'');
                    xlabel(app.UIAxes_15,'');
                    ylabel(app.UIAxes_15,'Salary (€)');
                    title(app.UIAxes_13,'Contract Types');
                    title(app.UIAxes_14,[Period 'Average Job Offers & Demands']);
                    title(app.UIAxes_15,'Salaries (Paris)');
                end   
            end
            
            for j=i+1:4
                if j==1
                    app.UITable.Data = table;  
                    app.UITable_2.Data = table;
                    app.UITable_9.Data = table;
                    app.Tab.Title = '';
                    % Delete UIAxes
                    delete(app.UIAxes)
                    delete(app.UIAxes_2)
                    delete(app.UIAxes_3)
                    app.Label.Text = '';
                end
                
                if j==2
                    app.UITable_3.Data = table; 
                    app.UITable_4.Data = table;
                    app.UITable_10.Data = table;
                    app.Tab_2.Title = '';
                    % Delete UIAxes
                    delete(app.UIAxes_4)
                    delete(app.UIAxes_5)
                    delete(app.UIAxes_6)
                    app.Label_2.Text = '';
                end
                
                if j==3
                    app.UITable_5.Data = table; 
                    app.UITable_6.Data = table;
                    app.UITable_11.Data = table;
                    app.Tab_4.Title = '';
                    % Delete UIAxes
                    delete(app.UIAxes_7)
                    delete(app.UIAxes_8)
                    delete(app.UIAxes_9)
                    app.Label_3.Text = '';
                end
                
                if j==4
                    app.UITable_7.Data = table; 
                    app.UITable_8.Data = table;
                    app.UITable_12.Data = table;
                    app.Tab_3.Title = '';
                    % Delete UIAxes
                    delete(app.UIAxes_10)
                    delete(app.UIAxes_11)
                    delete(app.UIAxes_12)
                    app.Label_4.Text = '';
                end
                
                if j==5
                    app.UITable_13.Data = table; 
                    app.UITable_14.Data = table;
                    app.UITable_15.Data = table;
                    app.Tab_5.Title = '';
                    % Delete UIAxes
                    delete(app.UIAxes_13)
                    delete(app.UIAxes_14)
                    delete(app.UIAxes_15)
                    app.Label_5.Text = '';
                end
            end
%             delete(app.Profile4Tab); %% this working
               
                         % Add data to the Table UI Component
            
        end
    end

    % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create TrouveTaVoieV07UIFigure and hide until all components are created
            app.TrouveTaVoieV07UIFigure = uifigure('Visible', 'off');
            app.TrouveTaVoieV07UIFigure.Color = [0.9412 0.9412 0.9412];
            app.TrouveTaVoieV07UIFigure.Position = [100 100 1409 739];
            app.TrouveTaVoieV07UIFigure.Name = 'TrouveTaVoie V0.7';

            % Create InputEditFieldLabel
            app.InputEditFieldLabel = uilabel(app.TrouveTaVoieV07UIFigure);
            app.InputEditFieldLabel.HorizontalAlignment = 'right';
            app.InputEditFieldLabel.FontWeight = 'bold';
            app.InputEditFieldLabel.Position = [292 704 35 22];
            app.InputEditFieldLabel.Text = 'Input';

            % Create InputEditField
            app.InputEditField = uieditfield(app.TrouveTaVoieV07UIFigure, 'text');
            app.InputEditField.Position = [342 704 759 22];

            % Create ScanButton
            app.ScanButton = uibutton(app.TrouveTaVoieV07UIFigure, 'push');
            app.ScanButton.ButtonPushedFcn = createCallbackFcn(app, @ScanButtonPushed, true);
            app.ScanButton.Position = [1143 704 100 22];
            app.ScanButton.Text = 'Scan';

            % Create TabGroup
            app.TabGroup = uitabgroup(app.TrouveTaVoieV07UIFigure);
            app.TabGroup.Position = [38 9 1365 680];

            % Create Tab
            app.Tab = uitab(app.TabGroup);

            % Create UITable
            app.UITable = uitable(app.Tab);
            app.UITable.ColumnName = {'Jobs'};
            app.UITable.RowName = {};
            app.UITable.Position = [11 341 435 302];

            % Create UITable_2
            app.UITable_2 = uitable(app.Tab);
            app.UITable_2.ColumnName = {'Skills'};
            app.UITable_2.RowName = {};
            app.UITable_2.Position = [464 341 435 302];

            % Create Label
            app.Label = uilabel(app.Tab);
            app.Label.VerticalAlignment = 'top';
            app.Label.Position = [16 234 1337 95];
            app.Label.Text = '';

            % Create UITable_9
            app.UITable_9 = uitable(app.Tab);
            app.UITable_9.ColumnName = {''};
            app.UITable_9.RowName = {};
            app.UITable_9.Position = [918 341 435 302];

            % Create Tab_2
            app.Tab_2 = uitab(app.TabGroup);

            % Create UITable_3
            app.UITable_3 = uitable(app.Tab_2);
            app.UITable_3.ColumnName = {'Jobs'};
            app.UITable_3.RowName = {};
            app.UITable_3.Position = [11 341 435 302];

            % Create UITable_4
            app.UITable_4 = uitable(app.Tab_2);
            app.UITable_4.ColumnName = {'Skills'};
            app.UITable_4.RowName = {};
            app.UITable_4.Position = [464 341 435 302];

            % Create Label_2
            app.Label_2 = uilabel(app.Tab_2);
            app.Label_2.VerticalAlignment = 'top';
            app.Label_2.Position = [16 234 1337 95];
            app.Label_2.Text = '';

            % Create UITable_10
            app.UITable_10 = uitable(app.Tab_2);
            app.UITable_10.ColumnName = {'Diplomas'};
            app.UITable_10.RowName = {};
            app.UITable_10.Position = [918 341 435 302];

            % Create Tab_4
            app.Tab_4 = uitab(app.TabGroup);

            % Create UITable_5
            app.UITable_5 = uitable(app.Tab_4);
            app.UITable_5.ColumnName = {'Jobs'};
            app.UITable_5.RowName = {};
            app.UITable_5.Position = [11 341 435 302];

            % Create UITable_6
            app.UITable_6 = uitable(app.Tab_4);
            app.UITable_6.ColumnName = {'Skills'};
            app.UITable_6.RowName = {};
            app.UITable_6.Position = [464 341 435 302];

            % Create Label_3
            app.Label_3 = uilabel(app.Tab_4);
            app.Label_3.VerticalAlignment = 'top';
            app.Label_3.Position = [16 234 1337 95];
            app.Label_3.Text = '';

            % Create UITable_11
            app.UITable_11 = uitable(app.Tab_4);
            app.UITable_11.ColumnName = {'Diplomas'};
            app.UITable_11.RowName = {};
            app.UITable_11.Position = [918 341 435 302];

            % Create Tab_3
            app.Tab_3 = uitab(app.TabGroup);

            % Create UITable_7
            app.UITable_7 = uitable(app.Tab_3);
            app.UITable_7.ColumnName = {'Jobs'};
            app.UITable_7.RowName = {};
            app.UITable_7.Position = [11 341 435 302];

            % Create UITable_8
            app.UITable_8 = uitable(app.Tab_3);
            app.UITable_8.ColumnName = {'Skills'};
            app.UITable_8.RowName = {};
            app.UITable_8.Position = [464 341 435 302];

            % Create Label_4
            app.Label_4 = uilabel(app.Tab_3);
            app.Label_4.VerticalAlignment = 'top';
            app.Label_4.Position = [16 234 1337 95];
            app.Label_4.Text = '';

            % Create UITable_12
            app.UITable_12 = uitable(app.Tab_3);
            app.UITable_12.ColumnName = {'Diplomas'};
            app.UITable_12.RowName = {};
            app.UITable_12.Position = [918 341 435 302];

            % Create Tab_5
            app.Tab_5 = uitab(app.TabGroup);

            % Create UITable_13
            app.UITable_13 = uitable(app.Tab_5);
            app.UITable_13.ColumnName = {'Jobs'};
            app.UITable_13.RowName = {};
            app.UITable_13.Position = [11 341 435 302];

            % Create UITable_14
            app.UITable_14 = uitable(app.Tab_5);
            app.UITable_14.ColumnName = {'Skills'};
            app.UITable_14.RowName = {};
            app.UITable_14.Position = [464 341 435 302];

            % Create Label_5
            app.Label_5 = uilabel(app.Tab_5);
            app.Label_5.VerticalAlignment = 'top';
            app.Label_5.Position = [16 234 1337 95];
            app.Label_5.Text = '';

            % Create UITable_15
            app.UITable_15 = uitable(app.Tab_5);
            app.UITable_15.ColumnName = {'Diplomas'};
            app.UITable_15.RowName = {};
            app.UITable_15.Position = [918 341 435 302];

            % Create Knob
            app.Knob = uiknob(app.TrouveTaVoieV07UIFigure, 'continuous');
            app.Knob.Position = [156 703 25 25];

            % Show the figure after all components are created
            app.TrouveTaVoieV07UIFigure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = RT2DJS_V0_5

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.TrouveTaVoieV07UIFigure)

            % Execute the startup function
            runStartupFcn(app, @startupFcn)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.TrouveTaVoieV07UIFigure)
        end
    end
end